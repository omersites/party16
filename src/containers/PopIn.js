import { useRef } from 'react'
import styled from 'styled-components'

// CSSTransition
import { CSSTransition } from 'react-transition-group'


const TransitionDiv = styled.div`
  opacity: 0;
  transform: scale(.9);
  pointer-events: none;
  transition: transform .2s, opacity .2s;

  &.pop-in-appear-active, &.pop-in-appear-done {
    pointer-events: all;
    opacity: 1;
    transform: unset;
  }

  &.pop-in-enter-active, &.pop-in-enter-done {
    pointer-events: all;
    opacity: 1;
    transform: unset;
  }
`

export default function PopIn({ children, inState = true, ...props }) {

  const nodeRef = useRef(null)

  return (
    <CSSTransition
      nodeRef={nodeRef} 
      in={inState}
      appear={true} 
      timeout={200}
      classNames="pop-in"
    >
      <TransitionDiv className='transition' ref={nodeRef} {...props}>
        {children}
      </TransitionDiv>
    </CSSTransition>
  )
}
