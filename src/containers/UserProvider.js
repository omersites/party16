import React, { useEffect, useState } from 'react'

// components
import Spinner from '../components/Spinner'

import { auth} from '../firebase'
import { onAuthStateChanged } from 'firebase/auth';

// context
export const UserContext = React.createContext([])

export default function UserProvider({ children }) {

  const [user, setUser] = useState(null)
  const [isPending, setIsPending] = useState(true)

  useEffect(() => {
    onAuthStateChanged(auth, (user) => {
      if (user) {
        setIsPending(false)
        setUser(user)
      } else {
        setIsPending(false)
      }
    });
  },[])

  return (
    <UserContext.Provider value={{ user, isPending }}>
        {/* pending */}
        {isPending && <Spinner />}
        {/* loaded */}
        {!isPending && children}
    </UserContext.Provider>
  )
}