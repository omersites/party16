// components
import BigImgBtn from '../components/BigImgBtn'

// images
import QRImg from '../assets/qr.svg'

export default function QRButton_unpaid() {
  return (
    <BigImgBtn
      disabled
      img={QRImg}
      tool-bottom="עליך לשלם בשביל לקבל ברקוד"
      tool-mobile=""
      style={{ '--color': '#eee', zIndex: '1'}}
      imgStyle={{ opacity: '.5'}}
    />
)
}