// components
import BigImgBtn from '../components/BigImgBtn'

// images
import CheckImg from '../assets/check.svg'

export default function ConfirmButton_confirm({ onClick }) {
  return (
    <BigImgBtn
      onClick={onClick}   
      img={CheckImg} 
      tool="לאישור הגעה"
      tool-bottom="לאישור הגעה"
      style={{ '--color': '#b8e5ff' }} 
      imgStyle={{ filter: 'invert(16%) sepia(59%) saturate(2959%) hue-rotate(182deg) brightness(93%) contrast(103%)' }}
    />
  )
}