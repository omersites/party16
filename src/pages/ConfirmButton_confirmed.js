// containers
import DelayRender from '../containers/DelayRender'
import FadeIn from '../containers/FadeIn'

// components
import BigImgBtn from '../components/BigImgBtn'
import ImgBtn from '../components/ImgBtn'

// images
import DoneImg from '../assets/done.svg'
import XImg from '../assets/x.svg'

export default function ConfirmButton_confirmed({ onClick }) {
  return (
    <BigImgBtn 
      disabled
      img={DoneImg}
      style={{'--color': '#9fffb5', position: 'relative'}}
      imgStyle={{
        filter: 'invert(15%) sepia(86%) saturate(4970%) hue-rotate(147deg) brightness(101%) contrast(105%)'
      }}
    >
      <DelayRender timeout={200}>
      <FadeIn>
        <ImgBtn 
          onClick={onClick} 
          img={XImg} 
          style={{
            zIndex: '2',
            position: 'absolute',
            top: '0',
            left: '0',
            backgroundColor: '#ffc7c7',
          }} 
          imgStyle={{
            filter: 'brightness(0)',
            opacity: '.2'
          }} 
          tool-bottom="ביטול אישור הגעה"
        />
      </FadeIn>
      </DelayRender>
    </BigImgBtn>
  )
}