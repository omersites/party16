// containers
import UserProvider from './containers/UserProvider' 

// components
import Main from './pages/Main';

export default function App() {
  return (
    <div className="App flex-column">
      <UserProvider>
        <Main />
      </UserProvider>
    </div>
  )
}