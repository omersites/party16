import { db } from "../firebase"
import { collection, doc, setDoc, deleteDoc, updateDoc, query, where, getDocs } from "firebase/firestore"

export default function useFirestore(collectionName) {

  const addDocument = data => {
    const newDocRef = doc(collection(db, collectionName))
    setDoc(newDocRef, {
      ...data,
      id: newDocRef.id
    })
  }

  const removeDocument = id => {
    deleteDoc(doc(db, collectionName, id));
  }

  const updateDocument = async (id, data) => {
    const docRef = doc(db, collectionName, id);
    return updateDoc(docRef, data);
  }

  const findDocument = async myQuery => {
    const q = query(collection(db, collectionName), where(...myQuery))
    const querySnapshot = await getDocs(q);
    return querySnapshot
  }

  return { addDocument, removeDocument, updateDocument, findDocument }
}