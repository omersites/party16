import { useState, useEffect } from 'react'
import { db } from "../firebase"
import { onSnapshot, collection } from "firebase/firestore"

export default function useFirestoreCollection(collectionName) {

  const [isPending, setIsPending] = useState(true)
  const [documents, setDocuments] = useState([])

  useEffect(() => {
    const unsubscribe = onSnapshot(collection(db, collectionName), (querySnapshot) => {
      setIsPending(false)
      const docsArr = []
      querySnapshot.forEach((doc) => {
        docsArr.push(doc.data())
      })
      setDocuments(docsArr)
    })
    return () => unsubscribe()
  }, [collectionName])

  return { documents, isPending }
}