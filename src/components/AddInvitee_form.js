import { useState } from "react"

// components
import Input from './Input'
import PlusBtn from "./ImgBtn"
import AddImg from '../assets/add.svg'

export default function AddInvitee_form({ onSubmit }) {

  const nameState = useState('')
  const phoneState = useState('')

  const handleSubmit = e => {
    e.preventDefault()
    if (nameState[0] && phoneState[0]) {
      onSubmit({
        name: nameState[0],
        phone: phoneState[0]
      })
    } else {
      alert('יש להכניס שם וטלפון!')
    }
  }

  return (
    <form onSubmit={handleSubmit}>
      <div 
        className="flex-column" 
        style={{ padding: '1em 0'}}
      >
        <Input type="text" state={nameState} placeholder="שם" />
        <Input type="number" state={phoneState} placeholder="טלפון" />
      </div>
      <PlusBtn 
        img={AddImg}
        tool-bottom="הוסף\י"
      />
    </form>
  )
}