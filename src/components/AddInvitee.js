import { useState } from 'react'

// components
import PlusBtn from './ImgBtn'
import Modal from './Modal'
import AddInvitee_form from './AddInvitee_form'
import AddImg from '../assets/add.svg'

// styles
import './AddInvitee.scss'

export default function AddInvitee({ addDocument }) {

  const [showModal, setShowModal] = useState(false)

  const handleSubmit = (data) => {
    const { name, phone } = data
    setShowModal(false)
    addDocument({
      name,
      phone,
      arriving: false,
      paid: false,
    })
  }

  return (
    <div className="add-invitee">
      <PlusBtn 
        img={AddImg}
        onClick={() => setShowModal(true)} 
        tool-top="הוספת מוזמנים"
      />
      <Modal showModal={showModal} setShowModal={setShowModal}>
        <AddInvitee_form onSubmit={handleSubmit} />
      </Modal>
    </div>
  )
}