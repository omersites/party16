// components
import ImgBtn from './ImgBtn'

// containers
import PopIn from '../containers/PopIn'

// images
import TumbleweedImg from '../assets/tumbleweed.svg'
import PaidImg from '../assets/paid.svg'
import CheckImg from '../assets/check.svg'
import XImg from '../assets/x.svg'
import PaidCheckImg from '../assets/price_check.svg'

// styles
import './List.scss'

export default function List({ listArr, isAdmin, onXClick, onPaid }) {
  return (
    <ul className="list reset-css flex-column">
      {listArr.length ? 
        listArr.map(item => (
          <li key={item.id}>
            {isAdmin && <button className="x-btn flex-center" onClick={() => onXClick(item.id)}><img src={XImg}/></button>}
            <div className="content">
              <span className="name">{item.name}</span>
              <span className="phone">{addDashes(item.phone)}</span>
            </div>
            <div className="flex-span"></div>
            {isAdmin && !item.paid && (
              <PopIn>
                <ImgBtn img={PaidCheckImg} onClick={() => onPaid(item.id, true)} style={{backgroundColor: '#9fffb5'}} imgStyle={{ filter: 'brightness(0)'}} tool-top="אישור תשלום"/>
              </PopIn>
            )}
            <div className="icons">
              <div tool-top="אישור הגעה" tool-mobile="">
                <img 
                  className={item.arriving ? 'filter-blue' : null} 
                  src={CheckImg} 
                />
              </div>
              {isAdmin ? (
                <div tool-top="אישור תשלום" tool-mobile="" onClick={() => onPaid(item.id, !item.paid)} className="clickable">
                  <img 
                    className={item.paid ? 'filter-green' : null} 
                    src={PaidImg}
                  />
                  {item.paid && <img src={XImg} className="x-img" />}
                </div>
              ) : (
                <div tool-top="אישור תשלום" tool-mobile="">
                  <img 
                    className={item.paid ? 'filter-green' : null} 
                    src={PaidImg}
                    />
                </div>
              )}
            </div>
          </li>
        ))
      :(
        <img src={TumbleweedImg} />
      )}
    </ul>
  )
}

const addDashes = phone => {
  const phone_val = phone.replace(/\D[^\.]/g, "");
  return phone_val.slice(0,3)+"-"+phone_val.slice(3,6)+"-"+phone_val.slice(6);
}